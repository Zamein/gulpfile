const gulp = require('gulp');
const sass = require('gulp-sass');
const cleanCSS = require('gulp-clean-css');
const autoprefixer = require('gulp-autoprefixer');
const jshint = require('gulp-jshint');
const concat = require("gulp-concat-js");
const imagemin = require('gulp-imagemin');

gulp.task('sass', function() {
    return gulp.src('css/style.scss')
        .pipe(sass())
        .pipe(autoprefixer('last 2 version', 'safari 5', 'ie 8', 'opera 12.1', 'ios 6', 'android 4'))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(''))
});

gulp.task('lint', function() {
    return gulp.src('js/script.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

gulp.task('scripts', function() {
    return gulp.src(['js/plugins.js', 'js/script.js'])
        .pipe(concat('script.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('js'));
});

gulp.task('images', function() {
    var imgSrc = 'img/source/*';
    var imgDest = 'img';

    return gulp.src(imgSrc)
        .pipe(newer(imgDest))
        .pipe(imagemin())
        .pipe(gulp.dest(imgDest));
});

gulp.task('watch', function() {

    // Listen on port 35729
    server.listen(35729, function (err) {
        if (err) {
            return console.log(err)
        };

        // Watch .js files
        gulp.watch('js/*.js', ['lint', 'scripts']);

        // Watch .scss files
        gulp.watch('**/*.scss', ['sass']);

        // Watch image files
        gulp.watch('img/source/*', ['images']);

    });

});