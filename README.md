# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Just a simple gulpfile for local website development.

### How do I get set up? ###

```
npm install --save-dev gulp-sass
npm install --save-dev gulp-clean-css
npm install --save-dev gulp-autoprefixer
npm install --save-dev gulp-jshint
npm install --save-dev gulp-concat-js
npm install --save-dev gulp-imagemin
```
